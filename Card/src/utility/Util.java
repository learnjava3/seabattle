package utility;
import java.io.IOException;
import java.util.Scanner;


public final class Util {
    
    
    public static final void print(String msg, Object... args){
        System.out.printf( "[CARD GAME] " + msg + "\n", args );
    }
    
   
    public static final void printDebug(String msg, Object... args){
        System.out.printf( "[DEBUG] " + msg + "\n", args );
    }
    
    
    public static final void printError(String msg, Object... args){
        System.out.printf( "[ERROR] " + msg + "\n", args);
    }
    
    
    public static final void printEmptyMessage( String msg ){
        System.out.println("<<< [" + msg + "] >>>");
    }
    
  
    public static final void printSeparator( String msg ){
        System.out.println("==================[" + msg + "]==================");
    }
    
   
    public static final void printSeparator2( String msg ){
        System.out.println(">>>>>>>>>>>>>>> [" + msg + "] <<<<<<<<<<<<<<<<");
    }
     
   
    public static void printInBox(String msg, Object... args) {
        String border = "";
        StringBuilder sbborder = new StringBuilder(border);
        for( int i = 0; i < msg.length(); i++ )
            sbborder.append("|");
        
        for( Object ob : args ) {
            String str;
            try {
                str = (String)ob;
            }catch(ClassCastException ex) { //  I know, cheeky. In this program, it's always going to be a int.
                str = Integer.toString((Integer)ob);
            }
            for( int i = 0; i < str.length(); i++ )
                sbborder.append("|");
        }
        
        border = sbborder.toString();
        
        System.out.println("||"+border+"||");
        System.out.printf("||| " + msg + " |||\n", args);
        System.out.println("||"+border+"||");
    }
    
  
  
    public static void clearConsoleConfirm(Scanner in){
        Util.print("Press ENTER to continue...");
        in.nextLine();
        clearConsole();
    }
    
    public static void clearConsole(){
        //Clears Screen in java
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ex) {}
    }
    
  
    private static Object stringToObject( String value ) {
        Object parsed_value = null;
        try{
            double isNum = Double.parseDouble(value);
            if(isNum == Math.floor(isNum)) {
                parsed_value = Integer.parseInt(value);
            }else {
                parsed_value = Double.parseDouble(value);
            }
        } catch(Exception e) {
            if(value.toCharArray().length == 1) {
                parsed_value = value.charAt(0);
            }else {
                if( "true".equals(value) ){
                    parsed_value = true;
                }else if( "false".equals(value) ){
                    parsed_value = false;
                }else if( "null".equals(value) ){
                    parsed_value = null;
                }else{
                    parsed_value = value;
                }
            }
        }      
        return parsed_value;
    }
    
   
    public static final Object promptInputValidationByValue( String msg, Scanner in, Object[] expected_values ){             
        Util.print("[PROMPT] " + msg);
        String value = in.nextLine().trim();
        Object parsed_value = stringToObject( value );
        
        boolean found = false;
        for( Object val : expected_values ){
            if( parsed_value.getClass().equals(val.getClass()) && parsed_value.equals(val) ){ // Avoid tricking java
                found = true;
                break;
            }
        }
        
        if( !found ){
            Util.printError("Error while parsing input! Please try again.");
            return promptInputValidationByValue(msg, in, expected_values);
        }
        
        return parsed_value;
    }
    

    public static final Object promptInputValidationByClass( String msg, Scanner in, Class<?> cl ){             
        Util.print("[PROMPT] " + msg);
        String value = in.nextLine().trim();
        Object parsed_value = stringToObject( value );
        
        if( !parsed_value.getClass().equals(cl) ){
            Util.printError("Error while parsing input! Please try again.");
            return promptInputValidationByClass(msg, in, cl);
        }
         
        return parsed_value;
    }
    

    public static final Object promptInputValidationByRange( String msg, Scanner in, int min, int max ){             
        Util.print("[PROMPT] " + msg);
        String value = in.nextLine().trim();
        int rvalue = -1;
        
        try{
            rvalue = Integer.parseInt(value);
        }catch( NumberFormatException e ){
            Util.printError("Your input was not a number!");
            return promptInputValidationByRange(msg, in, min, max);
        }
        
        if( rvalue < min || rvalue > max ){
            Util.printError("Your input: %d was not in the range of %d <= input <= %d", rvalue, min, max); 
            return promptInputValidationByRange(msg, in, min, max);
        }
        
        return rvalue;
    }
}
