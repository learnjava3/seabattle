package players;

import utility.Util;


public enum ComputerType {
    LOGICAL( "Logical", "This computer tries to play in a 'smart' way." ),
    RANDOM( "Random", "This player will play randomly." ),
    DUMB( "Dumb", "This player will play in a tunnel vision manner." );
    
    private final String name;
    private final String description;
    
  
    ComputerType( String name, String description ){
        this.name = name;
        this.description = description;
    }
    
   
    public static ComputerType getFromString( String str ){
        switch( str ){
            case "LOGICAL":
                return LOGICAL;
            case "RANDOM":
                return RANDOM;
            case "DUMB":
                return DUMB;   
            default:
                throw new Error("Invalid conversion from string " + str + " to Difficulty enum!");
        }
    }
   
  
    public String getDescription() {
        return description;
    }
    
   
    public String getName() {
        return name;
    }
    
 
    public static void printDescription() {
        Util.printSeparator2("Computer Types");
        for ( ComputerType typ : ComputerType.values() ) {
            System.out.println( typ.getName().toUpperCase() + " - " + typ.getDescription());
        }
    }
    
    
    public static String[] getTypesAsStrings() {
        ComputerType[] arr = ComputerType.values();
        String[] types = new String[arr.length*2];
        int i = 0;
        for ( ComputerType typ : arr ) {
            types[i] = typ.toString();
            i++;
        }
        for ( ComputerType typ : arr ) {
            types[i] = typ.toString().toLowerCase();
            i++;
        }
        return types;
    }
}
