package cards;
import java.util.ArrayList;

import utility.Util;

public class Graveyard {
    private ArrayList<Card> yard;
    
 
    public Graveyard(){
        yard = new ArrayList<Card>();
    }
    
  
    public void addCard( Card card ){
        yard.add(card);
        card.setInGraveyard(true);
        Util.print("%s was sent to the graveyard", card.getName());
    }

    public Card removeCard( int index ){
        Card card = yard.remove(index);
        card.setInGraveyard(false);
        return card;
    }

    public boolean containsCard( Card card ){
        return yard.contains(card);
    }
    
  
    public int numCardsGraveyard(){
        return yard.size();
    }
   
    public void printGraveyard(){
        int i = 0;
        for( Card c : yard ){
            if( c != null ){
                Util.print("Card Index on graveyard: %d", i);
                c.printCard();
                i++;
            }
        }
        if( i == 0 ){
            Util.printEmptyMessage("GRAVEYARD IS EMPTY");
        }
    }
}
