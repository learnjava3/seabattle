package cards;

import utility.Util;


public class Hand {
   
    public static final int MAX_CARDS_IN_HAND = 2; // this should remain as 2 - some logic depends on it :b (slightly lazy)
    private final Card array[];
    private int cards_in_hand;
   
    public Hand(){
        array = new Card[MAX_CARDS_IN_HAND];
        cards_in_hand = 0;
    }
    
 
    public void addCardToHand( Card card ){
        if ( cards_in_hand == MAX_CARDS_IN_HAND ) {
            throw new IllegalStateException("Hand is full!");
        }
        
        for( int i = 0; i < array.length; i++ ){
            if( array[i] == null ){ // find first free space and break
                array[i] = card;
                Util.printDebug("Added card %s to a hand at pos %d!", array[i].getName(), i);
                break;
            }
        }
       
        cards_in_hand++;
    }
 
    public Card removeCardFromHand( int index ){
        if ( cards_in_hand == 0 ) {
            throw new IllegalStateException("Hand is empty");
        } 
        
        if ( !hasCardOnIndex(index) ) {
            throw new IllegalStateException("There is no card on the hand at this index!");
        }
        
        Card cardremoved = array[index];
        array[index] = null;
        cards_in_hand--;
        
        return cardremoved;
    }
    
  
    public boolean hasCardOnIndex( int index ){
        return array[index] != null;
    }
    
    
    public int getCardsInHand(){
        return cards_in_hand;
    }
    
   
    public Card getCardFromHand( int index ){
        if ( !hasCardOnIndex(index) ) {
            throw new IllegalStateException("There is no card on the hand at this index!");
        }
        
        return array[index];
    }
    
 
    public int getFirstCardIndexFromHand() {
        return ( array[0] == null ) ? 1 : 0;
    }
    
 
    public void printCards() {
        int i = 0;
        for( Card c : array ){
            if( c != null ){
                Util.print("Card Index on hand: %d", i);
                c.printCard();
                i++;
            }
        }
        if( i == 0 ){
            Util.printEmptyMessage("HAND IS EMPTY");
        }
    }
    
  
    public void printCardsHidden() {
        int i = 0;
        for( Card c : array ){
            if( c != null ) {
                c.printCardHidden();
                i++;
            }
        }
        if( i == 0 ){
            Util.printEmptyMessage("HAND IS EMPTY");
        }
    }
    
}
