package cards;

public enum AttributeType {
    INFLICTING( "Inflicting", "This attribute type deals damage to the cards on the board" ),
    BUFF( "Buff", "This attribute type Buffs the card played on the board" ),
    RESILIANCE( "Resiliance", "This attribute type buffs the next card played on the board" );
    
    private final String name;
    private final String description;
    
  
    AttributeType( String name, String description ){
        this.name = name;
        this.description = description;
    }
 
    public String getName() {
        return name;
    }
    
    public String getDescription() {
        return description;
    }
    
    
    public static AttributeType getFromString( String str ){
        switch( str ){
            case "INFLICTING":
                return INFLICTING;
            case "BUFF":
                return BUFF;
            case "RESILIANCE":
                return RESILIANCE;   
            default:
                throw new Error("Invalid conversion from string " + str + " to AttributeType enum!");
        }
    }
}
