package cards;

import utility.Util;

public class Attribute {
    // Instance variables
    private String name;
    private String description;
    private AttributeType type;
    private int value;
    private int num_targets;
    // Instance constants
    private final boolean is_timed;
    private final int turns;
    

    public Attribute( String name, String description, int value, int num_targets, boolean is_timed, int turns, AttributeType type ){
        this.name = name;
        this.description = description;
        this.value = value;
        this.type = type;
        this.num_targets = num_targets;
        this.is_timed = is_timed;
        this.turns = turns;
    }

    public String getName() {
        return name;
    }
 
    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }
    
   
    public AttributeType getType(){
        return type;
    }
    
   
    public int getNumTargets() {
        return num_targets;
    }
    
    
    public boolean isTimed() {
        return is_timed;
    }
  
    public int getTurns() {
        return turns;
    }
    
   
    public void activate( Card activator, Card target ){
        switch( type ){
            case INFLICTING:
                int damage_after_resiliance = target.getResiliance()-value;
                if( damage_after_resiliance < 0 ){
                    target.setResiliance(0);
                }else{
                    target.setResiliance(damage_after_resiliance);
                    Util.print("%s has inflicted %d points of damage on %s, however, resiliance protected the target's power!", activator.getName(), value, target.getName());
                    break; // No need to continue
                }
                
                target.setPower(target.getPower()+damage_after_resiliance); // left over damage subtracts on power  
                Util.print("%s has inflicted %d points of damage on %s", activator.getName(), value, target.getName());
                break;
            case BUFF:
                target.setPower(target.getPower()+value);
                Util.print("%s has buffed %s by %d", activator.getName(), target.getName(), value);
                break;
            case RESILIANCE:
                target.setResiliance(target.getResiliance()+value);
                Util.print("%s has inscreased %s resiliance by %d", activator.getName(), target.getName(), value);
                break;
            default:
                throw new IllegalStateException("Invalid attribute type during activation!");
        }
        
        Util.print( "Attribute '%s' from the card %s was activated targeting the card %s", name, activator.getName(), target.getName() );
    }
}
