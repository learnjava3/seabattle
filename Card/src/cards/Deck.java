package cards;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import players.Player;
import utility.PresetDecks;
import utility.Util;


public class Deck implements Iterable<Card>{
    private static final int DEFAULT_DECK_SIZE = 5;
    private final int MAX_SIZE;
    private final Card[] array;
    private String name;
    private int size;
    private int front;
    private int rear;
    private Player owner;
    
 
    public Deck( String name, int MAX_SIZE ){
        this.MAX_SIZE = MAX_SIZE;
        this.name = name;
        this.size = 0;
        this.front = 0;
        this.rear = 0;
        array = new Card[MAX_SIZE];
        
        this.owner = null;
    }
 
    private void enqueue( Card card ){
        if( size == MAX_SIZE ){
            throw new IllegalStateException("Queue for deck is full!");
        }
        
        array[rear] = card;
        rear = (++rear % MAX_SIZE);
        size++;
    }
    
   
    private Card dequeue(){
        if( size == 0 ){
            throw new IllegalStateException("Queue for deck is empty!");
        }
        
        Card data = array[front%MAX_SIZE];
        array[front] = null;
        front = (++front % MAX_SIZE);
        size--;
        return data;
    }
  
    public Card removeCard(){
        Card card = dequeue();
        Util.printDebug("Removed card %s from deck %s", card.getName(), name);
        return card;
    }
    
 
    public void addCard( Card card ){
        enqueue(card);
        Util.printDebug("Added card %s to deck %s", card.getName(), name);
    }
    
  
    public int getCardsLeft(){
        return size;
    }
   
    public Player getOwner() {
        return owner;
    }
    
   
    public void setOwner(Player owner) {
        this.owner = owner;
    }
    
   
    public void shuffle(){
        Random rnd = ThreadLocalRandom.current();
        for (int i = array.length - 1; i > 0; i--) {
          int index = rnd.nextInt(i + 1);
          // Simple swap
          Card c = array[index];
          array[index] = array[i];
          array[i] = c;
        }
        Util.print("Deck %s has been shuffled!", name);
    }
    

    public static Deck loadPresetDeck( DeckFaction faction ){
        Deck deck = new Deck( faction.getName(), DEFAULT_DECK_SIZE );
        switch(faction){  
            case ELVES: 
                deck = PresetDecks.loadElvenDeck(deck);
                break;
            case PIRATES:
                deck = PresetDecks.loadPirateDeck(deck);
                break;
            case KINGDOM:
                deck = PresetDecks.loadKingdomDeck(deck);
                break;
        }
        Util.print("Finished loading deck preset '%s'", faction.getName() );
        return deck;
    }
    
  
    private final class DeckIterator implements Iterator<Card> {
        private int pointer;
        
        public DeckIterator(){
            pointer = rear;
        }
        
        @Override
        public boolean hasNext() {
            return pointer < front;
        }
        
        @Override
        public Card next() {
            if(this.hasNext()) {
               return array[++pointer];
            }
            throw new NoSuchElementException();
        }
    }
 
    @Override
    public Iterator<Card> iterator() {
        return new DeckIterator();
    }
}
